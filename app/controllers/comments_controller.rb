class CommentsController < ApplicationController
  def create
  	@comment = Comment.new(comment_params)
  	respond_to do |format|
  		if @comment.save
      	format.html { redirect_to @comment.entry, notice: 'Comentario creado exitosamente.' }
      end
    end
  end

  def destroy
  	@comment = Comment.find(params[:id])
  	@comment.destroy
    respond_to do |format|
      format.html { redirect_to @comment.entry, notice: 'Comentario eliminado exitosamente.' }
    end
  end

  private
  	def comment_params
      params.require(:comment).permit(:commenter, :body, :entry_id)
    end
end
