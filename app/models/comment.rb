class Comment < ApplicationRecord
  belongs_to :entry
  validates :entry, presence: true
  validates :body, presence: true
  validates :commenter, presence: true
end
