json.extract! entry, :id, :title, :subheader, :body, :created_at, :updated_at
json.url entry_url(entry, format: :json)
