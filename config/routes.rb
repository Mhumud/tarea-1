Rails.application.routes.draw do

  devise_for :users
  resources :entries
  resources :comments
  
  # get 'home/index'

  root to: "entries#index"
end
