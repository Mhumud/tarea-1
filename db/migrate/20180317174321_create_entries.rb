class CreateEntries < ActiveRecord::Migration[5.1]
  def change
    create_table :entries do |t|
      t.string :title, null: false, limit: 200
      t.string :subheader, null: false, limit: 200
      t.text :body, null: false
      t.integer :user_id

      t.timestamps
    end
  end
end
